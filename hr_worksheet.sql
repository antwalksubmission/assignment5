select * from student;

select * from course;

create table student (
    id number, 
    name varchar2(100),
    age number, 
    address varchar2(100)
);

insert into student values(1001,'Upasana',32,'Bengaluru');

drop table student;

alter table student add(phone long);
alter table student drop column phone;
alter table student modify name varchar2(200);

insert into student(id,name,age,address) values(1002,'Sachin',42,'Mumbai');

insert into student values(1003,'Virat',34,'Delhi',1111);
insert into student values(1004,'Rohit',30,'Mumbai',2222);
insert into student values(1005,'Rahul',31,'Bengaluru',3333);
insert into student values(1006,'Dravid',43,'Bengaluru',4444);

update student set phone =9999 where phone is null;
delete from table student where address = 'Kolkata';

select * from student where name like 'R_h%';

select distinct address from student;

insert into student(id,name,age,address) values(1007,'Steve',50,'Australia');

select * from student where phone is null;

select * from student where phone is not  null;

select address, avg(age) from student group by address;

select address, avg(age) from student group by address having avg(age)>=35;

drop table student;

desc course;

create table student (
id number generated  always as identity (start with 1 increment by 1),
name varchar(100), 
age number,
address varchar2(100),
course int references course(id)
);

drop table course;

create table course(id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1) PRIMARY KEY, description varchar2(100));


insert into course(id,description) values(3001,'Phy');
insert into course values(3002,'Phy');
insert into course values(3001,'Phy');
insert into course values(3001,'Phy');
insert into course(description) values('Maths');
insert into course(description) values('Phys');
insert into course(description) values('Bios');
insert into course(description) values('Chem');
insert into course(description) values('Economy');

select * from course;

delete from course;

--Find out the countries in a region.
--List all the people working in Europe
--List all the people working in Marketing 
and getting salary >10000
--list all the cities in America
--list all the people whose name start with S and working in Sales department
--list all the employees whose manager is Hunold

insert into student(name,age,address, course) values('Virat',34,'Delhi',8);
insert into student(name,age,address, course) values('Rohit',30,'Mumbai',9);
insert into student(name,age,address, course) values('Rahul',31,'Bengaluru',10);
insert into student(name,age,address, course) values('RahulDravid',43,'Bengaluru',6);
insert into student(name,age,address, course) values('RahulDravid',43,'Bengaluru',3010);
insert into student(name,age,address) values('Sehwag',43,'Delhi');

select * from student;
select * from REGIONS;
select * from DEPARTMENTS;
select * from COUNTRIES;
select * from LOCATIONS;
select * from EMPLOYEES;



select E.FIRST_NAME from EMPLOYEES E, DEPARTMENTS D 
where E.DEPARTMENT_ID = D.DEPARTMENT_ID and D.DEPARTMENT_ID in 
(select D.DEPARTMENT_ID from DEPARTMENTS D, LOCATIONS L 
where D.LOCATION_ID = L.LOCATION_ID and L.LOCATION_ID in 
(select L.LOCATION_ID from LOCATIONS  L, COUNTRIES C 
where L.COUNTRY_ID=C.COUNTRY_ID  
 and C.COUNTRY_ID in 
 (select C.COUNTRY_ID from COUNTRIES C, REGIONS R 
 where C.REGION_ID=R.REGION_ID 
 and R.REGION_NAME ='Europe' ))
 );

select C.COUNTRY_NAME, R.REGION_NAME from COUNTRIES C, REGIONS R where 
C.REGION_ID=R.REGION_ID;

select * from EMPLOYEES E, DEPARTMENTS D where E.DEPARTMENT_ID =D.DEPARTMENT_ID
and D.DEPARTMENT_NAME = 'Marketing';

select L.CITY from LOCATIONS L where L.COUNTRY_ID = 'US';

select * from EMPLOYEES E, DEPARTMENTS D where E.FIRST_NAME like 'S%' and E.DEPARTMENT_ID = D.DEPARTMENT_ID and D.DEPARTMENT_NAME = 'Sales';

select E1.FIRST_NAME from EMPLOYEES E1, EMPLOYEES E2 where E1.MANAGER_ID =E2.EMPLOYEE_ID and E2.LAST_NAME = 'Hunold';

