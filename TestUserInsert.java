package Domain;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class TestUserInsert {
    public static void main(String[] args) {
        String urlOracle = "jdbc:oracle:thin:@localhost:1521:orcl";
        String username = "system";
        String password = "Guddu06@nriF";
        Scanner sc = new Scanner(System.in);

        Connection con;

        {
            try {
                con = DriverManager.getConnection(urlOracle,username,password);
                System.out.println("Connection created successfully");
                String choice;
                do{
                    System.out.println("Enter new record");
                    System.out.print("name: \t");
                    String name = sc.nextLine();
                    System.out.print("age: \t");
                    int age = Integer.parseInt(sc.nextLine());
                    System.out.print("address: \t");
                    String address = sc.nextLine();
                    System.out.println("Course_id (6,7,8,9,10): \t");
                    int course = Integer.parseInt(sc.nextLine());
                    String values = "("+"'"+name+"'"+","+age+","+"'"+address+"'"+","+course+")";
                    String query = "insert into student(name,age,address, course) values"+values;
                    Statement stmt = con.createStatement();
                    stmt.executeUpdate(query);
                    System.out.println(values);
                    System.out.println("Press 1 to insert more records 0 to exit");
                    choice = sc.nextLine();
                }while (choice.equals("1"));


                System.out.println("Data inserted successfully");
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                throw new RuntimeException(e);
            }
        }
    }
}
