package Domain;

import java.sql.*;
import java.util.Scanner;

public class UserUpdate {
    public static void main(String[] args) {

        String urlOracle = "jdbc:oracle:thin:@localhost:1521:orcl";
        String username = "system";
        String password = "Guddu06@nriF";

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;


        try {
            // Connect to the database
            connection = DriverManager.getConnection(urlOracle, username, password);

            // Create a statement
            statement = connection.createStatement();
            System.out.println("Connection created successfully");


            // Read user input
            Scanner scanner = new Scanner(System.in);

            System.out.print("Enter the id of the entry to be updated: ");
            int id = Integer.parseInt(scanner.nextLine());

            System.out.println("Enter the attribute to be updated: ");
            String attribute = scanner.nextLine().toLowerCase();

            String value;
            int val;
            String updateQuery;
            if(attribute.equals("age")||attribute.equals("course"))
            {
                System.out.println("Enter integer value: ");
                val=Integer.parseInt(scanner.nextLine());
                updateQuery="UPDATE student SET "+attribute+" = '" + val + "' WHERE id = " + id;
//                System.out.println(updateQuery);
            }
            else {
                System.out.println("Enter string value: ");
                value=scanner.nextLine();
                updateQuery = "UPDATE student SET "+attribute+" = '" + value + "' WHERE id = " + id;
//                System.out.println(updateQuery);

            }

            // Create an update query

            // Execute the update query
            int rowsAffected = statement.executeUpdate(updateQuery);
            System.out.println(rowsAffected + " row(s) updated");
        }
        catch(SQLException e)
        {
            System.out.println(e.getMessage());
        }
    }
}